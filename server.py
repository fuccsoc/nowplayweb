import spotipy
from flask import Flask, render_template
import json

sp = spotipy.Spotify(
    auth_manager=spotipy.SpotifyOAuth(
        client_id="e0708753ab60499c89ce263de9b4f57a",
        client_secret="80c927166c664ee98a43a2c0e2981b4a",
        redirect_uri="https://fuccsoc.com/",
        scope=[
            "user-library-read",
            "user-read-playback-state",
            "user-read-currently-playing",
        ],
    )
)

app = Flask(__name__)


@app.route("/")
def main():
    try:
        playback_info = sp.current_playback()
        try:
            album_picture_url = playback_info["item"]["album"]["images"][0]["url"]
        except:
            album_picture_url = ""
        try:
            track_name = playback_info["item"]["name"]
        except:
            track_name = ""
        try:
            album_name = playback_info["item"]["album"]["name"]
        except:
            album_name = ""
        try:
            artist_name = ", ".join(
                [a["name"] for a in playback_info["item"]["artists"]]
            )
        except:
            artist_name = ""
    except:
        album_picture_url = None
        track_name = None
        album_name = None
        artist_name = None
    return render_template(
        "index.html",
        album_picture_url=album_picture_url,
        track_name=track_name,
        album_name=album_name,
        artist_name=artist_name,
    )


if __name__ == "__main__":
    app.run()
